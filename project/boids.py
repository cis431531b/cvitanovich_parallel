from graphics import *
import random
import math

BOIDSIZE = 25
BOIDWIDTH = 3
NBOIDS = 30
CENTERW = 0.5
AVOIDW = 0.8
VIEWW = 0.6
COPYW = 0.8
COLOR = "White"
COLOR2 = "Red"

PI = 3.1415926
dt = 3.0 # time step
ddt = 0.95
rcopy = 80; rcent = 70; rview = 40; rvoid = 15;
maxr = max(rview, max(rcopy, max(rcent, rvoid)))
minv = 0.5
wrand = 0

# boid viewing angles in radians
angle = 270.0
vangle = 90.0
angle = angle * PI / 180.0
vangle = vangle * PI / 180.0
cosangle = math.cos(angle / 2)
cosvangle = math.cos(vangle / 2)

W = 1024; H = 800; x = 0; y = 0;
win = GraphWin(width = W, height = H) # create a window
win.setBackground("Black")
win.setCoords(0, 0, W, H) # set the coordinates of the window; bottom left is (0, 0) and top right is (W, H)


def len(x, y):
    return math.sqrt( x * x + y * y )

def dist(x0, y0, x1, y1):
    return len(x0-x1,y0-y1)

def dot(x0,y0,x1,y1):
    return (x0*x1 + y0*y1)

# Boid class
class Boid(object):
    """docstring for ."""
    def __init__(self, color, window, count):
        self.p = [ random.uniform(0,W), random.uniform(0,H) ]
        self.size = BOIDSIZE
        self.v = [ random.uniform(-1,1), random.uniform(-1,1) ]
        self.newv = [0, 0]
        self.v_avoid = [0, 0]
        self.avoid_distance = 5
        self.v_copy = [0, 0]
        self.v_center = [0, 0]
        self.v_view = [0, 0]
        self.window = window
        self.num = count
        self.color  = color
        #self.plotArrow()


    def plotArrow(self):
        x = self.p[0] - self.v[0] * self.size
        y = self.p[1] - self.v[1] * self.size
        self.image0 = Line(Point(self.p[0], self.p[1]), Point(x,y))
        self.image0.setFill(self.color)
        self.image0.setWidth(BOIDWIDTH)
        self.image0.draw(self.window)

        t = (self.p[0] - x) / self.size
        t = -1 if (t < -1) else ( 1 if (t > 1) else t )
        a = math.acos(t)
        a = -a if (self.p[1] - y) < 0 else a

        x = self.p[0] + math.cos(a + angle / 2) * self.size / 3.0
        y = self.p[1] + math.sin(a + angle / 2) * self.size / 3.0
        self.image1 = Line(Point(self.p[0],self.p[1]), Point(x,y))
        self.image1.setFill(self.color)
        self.image1.setWidth(BOIDWIDTH)
        self.image1.draw(self.window)

        x = self.p[0] + math.cos(a - angle / 2) * self.size / 3.0
        y = self.p[1] + math.sin(a - angle / 2) * self.size / 3.0
        self.image2 = Line(Point(self.p[0],self.p[1]), Point(x,y))
        self.image2.setFill(self.color)
        self.image2.setWidth(BOIDWIDTH)
        self.image2.draw(self.window)

    def refresh(self):
        self.image0.undraw()
        self.image1.undraw()
        self.image2.undraw()
        self.plotArrow()


    def print(self):
        print("Boid #{} - pos({},{}) - velocity({},{})".format(self.num,self.p[0],self.p[1],self.v[0],self.v[1]))

class Flock:
    def __init__(self, size, color, window):
        self.size = size
        self.window = window
        self.mu = 0.5 # momentum factor
        self.w_avoid =  AVOIDW
        self.w_view = VIEWW
        self.w_center = CENTERW
        self.w_copy = COPYW
        self.v_average = [0, 0]
        self.p_average = [0, 0]
        self.boidList = []
        for i in range(0,self.size):
            self.boidList.append( Boid(color,window,i) )
            self.boidList[-1].plotArrow()
            #self.boidList[-1].print()

    def refresh(self):
        for i in range(0,self.size):
            self.boidList[i].refresh()
            #self.boidList[i].print()

    def calcHeadings(self):

        for i in range(0,self.size):
            numcent = 0
            boid = self.boidList[i]
            boid.v_avoid = [0, 0]
            boid.v_copy = [0, 0]
            boid.v_center = [0, 0]
            boid.v_view = [0, 0]

            for j in range(0, self.size):

                # skip the identical boid
                if(j == i):
                    continue

                # find minimum distance between boids (wrap around screen)
                mindist = 10e10
                for k in range(-W,W,W):
                    for l in range(-H,H,H):
                        d = dist( self.boidList[j].p[0] + k, \
                            self.boidList[j].p[1] + l, boid.p[0], boid.p[1])
                        if( d < mindist ):
                            mindist = d
                            mx = self.boidList[j].p[0] + k
                            my = self.boidList[j].p[1] + l

                # skip boid if further than rule radii
                if(mindist > maxr):
                    continue

                # vector between boids
                temp = [0, 0]
                temp[0] = mx - boid.p[0]
                temp[1] = my - boid.p[1]

                # is this boid in first boid's field of view?
                # cosine of vector pointing between boids and first boid's velocity vector
                costemp = dot(boid.v[0], boid.v[1],temp[0], temp[1]) / (len(boid.v[0], boid.v[1]) * len(temp[0],temp[1]))
                if(costemp < cosangle):
                    continue;

                # center in on boid if within center rule radius but outside avoid radius
                if(mindist <= rcent and mindist > rvoid):
                    boid.v_center[0] += mx - self.boidList[j].p[0]
                    boid.v_center[1] += my - self.boidList[j].p[1]
                    numcent += 1

                # if within copy radius but outside avoid radius copy boid's velocity
                if(mindist <= rcopy and mindist > rvoid):
                    boid.v_copy[0] += self.boidList[j].v[0]
                    boid.v_copy[1] += self.boidList[j].v[1]

                # within collision range, so avoid the other boid
                if(mindist <= rvoid):
                    # calulate avoidance vector
                    xtemp = boid.p[0] - mx
                    ytemp = boid.p[1] - my
                    # avoidance vector magnitude proportional to distance
                    d = 1 / len(xtemp,ytemp)
                    xtemp = xtemp * d
                    ytemp = ytemp * d
                    boid.v_avoid[0] += xtemp
                    boid.v_avoid[1] += ytemp

                # try to move so boid does not block view
                if(mindist <= rview and cosvangle < costemp):
                    xtemp = boid.p[0] - mx
                    ytemp = boid.p[1] - my
                    u = 0
                    v = 0
                    if(xtemp != 0 and ytemp != 0):
                        u = math.sqrt(math.pow(ytemp / xtemp, 2) / (1 + math.pow(ytemp / xtemp, 2)))
                        v = -xtemp * u / ytemp
                    elif(xtemp != 0):
                        u = 1
                    elif(ytemp != 0):
                        v = 1
                    if(boid.v[0] * u + boid.v[1] * v < 0):
                        u = -u
                        v = -v

                    # add vector to move away from this boid
                    u = boid.p[0] - mx + u
                    v = boid.p[1] - my + v

                    # inversely proportional to distance
                    d = len(xtemp, ytemp)
                    if(d != 0):
                        u = u / d
                        v = v / d
                    boid.v_view[0] += u
                    boid.v_view[1] += v

            # avoid centering on just one boid
            if(numcent < 2):
                boid.v_center = [0, 0]

            #normalize vectors
            l = len(boid.v_center[0],boid.v_center[1])
            if(l > 1.0):
                boid.v_center[0] /= l
                boid.v_center[1] /= l

            l = len(boid.v_avoid[0],boid.v_avoid[1])
            if(l > 1.0):
                boid.v_avoid[0] /= l
                boid.v_avoid[1] /= l

            l = len(boid.v_view[0],boid.v_view[1])
            if(l > 1.0):
                boid.v_view[0] /= l
                boid.v_view[1] /= l

            l = len(boid.v_copy[0],boid.v_copy[1])
            if(l > 1.0):
                boid.v_copy[0] /= l
                boid.v_copy[1] /= l

            # compute weighted trajectory
            xt = boid.v_center[0] * theFlock.w_center + boid.v_copy[0] * \
                theFlock.w_copy + boid.v_avoid[0] * theFlock.w_avoid + \
                boid.v_view[0] * theFlock.w_view

            yt = boid.v_center[0] * theFlock.w_center + boid.v_copy[0] * \
                theFlock.w_copy + boid.v_avoid[0] * theFlock.w_avoid + \
                boid.v_view[0] * theFlock.w_view

            # add noise
            if(wrand > 0):
                xt += random.uniform(-1,1) * wrand
                yt += random.uniform(-1,1) * wrand

            # update velocity and renormalize
            boid.newv[0] = boid.v[0] * ddt + xt * (1 - ddt)
            boid.newv[1] = boid.v[1] * ddt + yt * (1 - ddt)
            d = len(boid.newv[0], boid.newv[1])
            if (d < minv):
                boid.newv[0] *= minv / d
                boid.newv[1] *= minv / d

theFlock = Flock(NBOIDS,COLOR,win)
theFlock2 = Flock(10,COLOR2,win)


# randomized initial conditions for each boid
for i in range(0, theFlock.size ):
    theFlock.boidList[i].p[0] = random.uniform(0,W)
    theFlock.boidList[i].p[1] = random.uniform(0,H)
    vx = random.uniform(-1,1)
    vy = random.uniform(-1,1)
    if(len(vx,vy) != 0):
        vx = vx / len(vx,vy)
        vy = vy / len(vx,vy)
    theFlock.boidList[i].v[0] = vx
    theFlock.boidList[i].v[1] = vy

for i in range(0,theFlock2.size):
    theFlock2.boidList[i].p[0] = random.uniform(0,W)
    theFlock2.boidList[i].p[1] = random.uniform(0,H)
    vx = random.uniform(-1,1)
    vy = random.uniform(-1,1)
    if(len(vx,vy) != 0):
        vx = vx / len(vx,vy)
        vy = vy / len(vx,vy)
    theFlock2.boidList[i].v[0] = vx
    theFlock2.boidList[i].v[1] = vy

# loop until quit key pressed
while(1):
    key = win.checkKey()
    if(key == 'q'):
        break


    # compute new headings for each boid
    theFlock.calcHeadings()
    theFlock2.calcHeadings()

    #win.getMouse()
    # update velocity and position vectors for each boid
    for i in range(0, theFlock.size ):
        theFlock.boidList[i].v[0] = theFlock.boidList[i].newv[0]
        theFlock.boidList[i].v[1] = theFlock.boidList[i].newv[1]
        #print("New velocity for Boid #{} is ({},{})".format(i, theFlock.boidList[i].v[0], theFlock.boidList[i].v[1]))
        theFlock.boidList[i].p[0] += theFlock.boidList[i].v[0] * dt
        theFlock.boidList[i].p[1] += theFlock.boidList[i].v[1] * dt

        # wrap coordinates
        if(theFlock.boidList[i].p[0] < 0):
            theFlock.boidList[i].p[0] += W
        elif(theFlock.boidList[i].p[0] >= W):
            theFlock.boidList[i].p[0] -= W
        if(theFlock.boidList[i].p[1] < 0):
            theFlock.boidList[i].p[1] += H
        elif(theFlock.boidList[i].p[1] >= H - 1):
            theFlock.boidList[i].p[1] -= H

    for i in range(0, theFlock2.size ):
        theFlock2.boidList[i].v[0] = theFlock2.boidList[i].newv[0]
        theFlock2.boidList[i].v[1] = theFlock2.boidList[i].newv[1]
        #print("New velocity for Boid #{} is ({},{})".format(i, theFlock2.boidList[i].v[0], theFlock2.boidList[i].v[1]))
        theFlock2.boidList[i].p[0] += theFlock2.boidList[i].v[0] * dt
        theFlock2.boidList[i].p[1] += theFlock2.boidList[i].v[1] * dt

        # wrap coordinates
        if(theFlock2.boidList[i].p[0] < 0):
            theFlock2.boidList[i].p[0] += W
        elif(theFlock2.boidList[i].p[0] >= W):
            theFlock2.boidList[i].p[0] -= W
        if(theFlock2.boidList[i].p[1] < 0):
            theFlock2.boidList[i].p[1] += H
        elif(theFlock2.boidList[i].p[1] >= H - 1):
            theFlock2.boidList[i].p[1] -= H

    # redraw boids
    theFlock.refresh()
    theFlock2.refresh()
