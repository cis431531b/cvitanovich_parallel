// Assign 3 -- serial version -- Andrew Cvitanovich
#include <vector>
#include <chrono>
#include <iostream>
#include <iomanip>
#include <stdio.h>
#include <stdlib.h>
#define ITER_COUNT 10000

int main(int argc, char *argv[]) {

  if(argc < 2) {
	std::cout << "Must provide array size n as first argument!\n";
	exit(0);
  }

  int n = atoi(argv[1]); // rows, cols of nxn input
  std::cout << "n = " << n << " ";
  int i,j;
  typedef std::vector< std::vector<double>> Block; // typedef for temp matrices


  // dynamic 2D arrays
  Block input_temp(n+2, std::vector<double> (n+2, 0));
  Block output_temp(n+2, std::vector<double> (n+2, 0));

  double c = 0.1;
  double ds = 1 / ((double) n + 1);
  double dt = (ds * ds) / (4 * c);
  double k = c * (dt / (ds * ds));
  double boundary_val = 100;
  double inner_val = -100;
  int niters = ITER_COUNT;

  typedef std::chrono::high_resolution_clock Clock;

  // initialize boundary values for input_temp and output_temp (100)
  // initialize inner values for input_temp (-100)
  for (i = 0; i < n+2; ++i) {
    for (j = 0; j < n+2; ++j) {
      if( i == 0 || i == n+1 || j == 0 || j == n+1) {
        input_temp[i][j] = boundary_val;
        output_temp[i][j] = boundary_val;
      } else {
        input_temp[i][j] = inner_val;
      }
    }
  }

  // solve 2D heat equation iteratively
  auto t1 = Clock::now();
  for (int iter = 0; iter < niters; ++iter) {


    for (i = 1; i < n+1; ++i) {
      for (j = 1; j < n+1; ++j) {
        output_temp[i][j] =
          input_temp[i][j] + k *
          (input_temp[i+1][j] + input_temp[i-1][j] -
           ( 4 * input_temp[i][j] ) +
           input_temp[i][j+1] + input_temp[i][j-1]
          );
      }
    }

    if (iter < (niters - 1) ) {
      // swap temp matrices
      for(i = 0; i < n+2; ++i) {
        std::vector<double> &v1 = input_temp[i];
        std::vector<double> &v2 = output_temp[i];
        v1.swap(v2);
      }
    }
  }



  // write output_temp?
  std::cout << "Completed:\n\n";
  double asum = 0;
  std::cout << std::setprecision(10);
  for (i = 1; i < n+1; i++) {
    for (j = 1; j < n+1; j++) {
      asum += output_temp[i][j];
    }
  }

  // print sum of final array
  std::cout << "SUM = " << asum << std::endl;


  auto t2 = Clock::now();

  std::cout << "Runtime: "
  << std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count()
  << " ms" << std::endl;

}
