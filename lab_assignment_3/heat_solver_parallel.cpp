// Assign 3 -- parallel version -- Andrew Cvitanovich
#include "mpi.h"
#include <vector>
#include <chrono>
#include <iostream>
#include <iomanip>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define ITER_COUNT 10000
#define MASTER 0
#define DEBUG 0

int main(int argc, char *argv[]) {

  if(argc < 2) {
	  std::cout << "Must provide array size n as first argument!\n";
	  exit(0);
  }

  int nprocs; // number of processes
  int pid; // id for processes
  int n = atoi(argv[1]); // rows, cols of nxn input
  int i,j;
  double * temp_row;

  typedef std::vector< std::vector<double>> Block;

  double c = 0.1;
  double ds = 1 / ((double) n + 1);
  double dt = (ds * ds) / (4 * c);
  double k = c * (dt / (ds * ds));
  double boundary_val = 100;
  double inner_val = -100;
  int niters = ITER_COUNT;

  typedef std::chrono::high_resolution_clock Clock;



  // initialize processes
  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
  MPI_Comm_rank(MPI_COMM_WORLD, &pid);
  std::cout << "MPI process " << pid << " processing subarray ...\n";

  // master process (pid == MASTER)
  if (pid == MASTER) {
    MPI_Status status;
    double sum = 0;
    // print array size
    std::cout << "n = " << n << "\n";

    // if only one process just run serial code
    if(nprocs == 1) {

      // dynamic 2D arrays
      Block input_temp(n+2, std::vector<double> (n+2, 0));
      Block output_temp(n+2, std::vector<double> (n+2, 0));

      // initialize boundary values for input_temp and output_temp (100)
      // initialize inner values for input_temp (-100)
      for (i = 0; i < n+2; ++i) {
        for (j = 0; j < n+2; ++j) {
          if( i == 0 || i == n+1 || j == 0 || j == n+1) {
            input_temp[i][j] = boundary_val;
            output_temp[i][j] = boundary_val;
          } else {
            input_temp[i][j] = inner_val;
          }
        }
      }

      // solve 2D heat equation iteratively
      auto t1 = Clock::now();
      for (int iter = 0; iter < niters; ++iter) {


        for (i = 1; i < n+1; ++i) {
          for (j = 1; j < n+1; ++j) {
            output_temp[i][j] =
              input_temp[i][j] + k *
              (input_temp[i+1][j] + input_temp[i-1][j] -
               ( 4 * input_temp[i][j] ) +
               input_temp[i][j+1] + input_temp[i][j-1]
              );
          }
        }

        if (iter < (niters - 1) ) {
          // swap temp matrices
          for(i = 0; i < n+2; ++i) {
            std::vector<double> &v1 = input_temp[i];
            std::vector<double> &v2 = output_temp[i];
            v1.swap(v2);
          }
        }
      }



      // write output_temp?
      std::cout << "Completed:\n\n";
      double asum = 0;
      std::cout << std::setprecision(10);
      for (i = 1; i < n+1; i++) {
        for (j = 1; j < n+1; j++) {
          asum += output_temp[i][j];
        }
      }

      // print sum of final array
      std::cout << "SUM = " << asum << std::endl;


      auto t2 = Clock::now();

      std::cout << "Runtime: "
      << std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count()
      << " ms" << std::endl;


      MPI_Finalize();
      return 0;

    }

    // more than one process

    // track completion of MPI message passing
    MPI_Request recv_req, send_req;

    // allocate subarray
    // master process calculates temps for it's portion
    int nrows = floor( (n+2) / nprocs ) + 1; // 1 extra row for ghost cells

    Block input_temp(nrows, std::vector<double> (n+2, 0));
    Block output_temp(nrows, std::vector<double> (n+2, 0));

    if(DEBUG) std::cout << "proc " << pid << " nrows " << nrows << std::endl;

    // initialize values
    for (i = 0; i < nrows; ++i) {
      for (j = 0; j < n+2; ++j) {
        if ( i == 0 || j == 0 || j == n+1) {
          input_temp[i][j] = boundary_val;
          output_temp[i][j] = boundary_val;
        } else {
          input_temp[i][j] = inner_val;
        }
      }
    }

    // solve 2D heat equation iteratively for subarray
    auto t1 = Clock::now();
    for (int iter = 0; iter < niters; ++iter) {

      // no need to solve for last row (ghost cells)
      // no need to solve for first row (boundary)
      for (i = 1; i < (nrows - 1); ++i) {
        if( iter > 0 && i == (nrows - 2) )
          MPI_Wait(&recv_req, &status); // wait if ghost cells not updated yet!
        for (j = 1; j < n+1; ++j) {
          output_temp[i][j] =
            input_temp[i][j] + k *
            (input_temp[i+1][j] + input_temp[i-1][j] -
             ( 4 * input_temp[i][j] ) +
             input_temp[i][j+1] + input_temp[i][j-1]
            );
        }
      }


      // no need for send or receive on last iteration
      if (iter < (niters - 1) ) {

        // swap temp matrices
        for(i = 0; i < nrows; ++i) {
          std::vector<double> &v1 = input_temp[i];
          std::vector<double> &v2 = output_temp[i];
          v1.swap(v2);
        }

        if(not DEBUG) {

          // sync with pid == 1
          // send results from penultimate row to pid == 1
          temp_row = &input_temp[nrows - 2][0];
          if (DEBUG) std::cout << "PID " << pid << " Send row" << std::endl;
          MPI_Isend(temp_row, n+2, MPI_DOUBLE, 1, 0, MPI_COMM_WORLD, &send_req);
          // update ghost cells (last row) using MPI_recv (pid == 1)
          if (DEBUG) std::cout << "PID " << pid << " Recv row" << std::endl;
          temp_row = &input_temp[nrows - 1][0];
          MPI_Irecv(temp_row, n+2, MPI_DOUBLE, 1, 0, MPI_COMM_WORLD, &recv_req);
          if (DEBUG) std::cout << "PID " << pid << " Done recv" << std::endl;

        }



      }

    }



    if(not DEBUG) {
      // combine results
      std::vector<double> temp_vec(n+2, 0);

      // receive rows from other processes

      // skip first two rows to avoid double counting overlapping rows from ghost cells
      int nrows_receive = floor( (n+2) / nprocs ) + 2;

      for (int proc = 1; proc < nprocs; ++proc) {

        // for last process the number of rows is different
        if( proc == (nprocs - 1) )
          nrows_receive = floor( (n+2) / nprocs ) + ( (n+2) % nprocs ) + 1;

        for (int k = 2; k < nrows_receive; ++k) {
          temp_row = &temp_vec[0];
          if (DEBUG) std::cout << "PID " << pid << " Recv row " << k << " at Master for " << proc << " k = " << k << std::endl;
          MPI_Recv(temp_row, n+2, MPI_DOUBLE, proc, 0, MPI_COMM_WORLD, &status);
          output_temp.push_back(temp_vec);
        }


      }

      // write output_temp?
      std::cout << "Completed:\n\n";
      double asum = 0;
      std::cout << std::setprecision(10);
      for (i = 1; i < n+1; i++) {
        for (j = 1; j < n+1; j++) {
          asum += output_temp[i][j];
        }
      }

      // print sum of final array
      std::cout << "SUM = " << asum << std::endl;

    }



    auto t2 = Clock::now();

    std::cout << "Runtime: "
    << std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count()
    << " ms" << std::endl;



  } else if (pid > MASTER && pid < (nprocs - 1) ) {

    MPI_Status status;

    // track completion of MPI message passing
    MPI_Request recv_req1, recv_req2, send_req1, send_req2;

    // other processes calculate temps for their portion
    // nrows
    int nrows = floor( (n+2) / nprocs ) + 2; // 2 extra rows for ghost cells
    Block input_temp(nrows, std::vector<double> (n+2, 0));
    Block output_temp(nrows, std::vector<double> (n+2, 0));

    if(DEBUG) std::cout << "proc " << pid << " nrows " << nrows << std::endl;

    // initialize values
    for (i = 0; i < nrows; ++i) {
      for (j = 0; j < n+2; ++j) {
        // in the middle of the temperature matrix, only need to do west/east boundaries
        if ( j == 0 || j == n+1 ) {
          input_temp[i][j] = boundary_val;
          output_temp[i][j] = boundary_val;
        } else {
          input_temp[i][j] = inner_val;
        }
      }
    }

    // solve for subarray

    for (int iter = 0; iter < niters; ++iter) {

      // no need to solve for first row (ghost cells) or last row (ghost cells)

      if(iter > 0)
        MPI_Wait(&recv_req1,&status); // wait for top ghost cells

      for (i = 1; i < (nrows - 1); ++i) {
        if( iter > 0 && i == (nrows - 2) )
          MPI_Wait(&recv_req2, &status); // wait for bottom ghost cells
        for (j = 1; j < n+1; ++j) {
          output_temp[i][j] =
            input_temp[i][j] + k *
            (input_temp[i+1][j] + input_temp[i-1][j] -
             ( 4 * input_temp[i][j] ) +
             input_temp[i][j+1] + input_temp[i][j-1]
            );
        }
      }

      // no need for send or receive on last iteration
      if (iter < (niters - 1) ) {

        // swap temp matrices
        for(i = 0; i < nrows; ++i) {
          std::vector<double> &v1 = input_temp[i];
          std::vector<double> &v2 = output_temp[i];
          v1.swap(v2);
        }

        if(not DEBUG) {
          // sync with pid - 1
          // update top ghost cells from pid - 1 using MPI_recv
          temp_row = &input_temp[0][0];
          MPI_Irecv(temp_row, n+2, MPI_DOUBLE, (pid - 1), 0, MPI_COMM_WORLD, &recv_req1);
          // send results of 2nd row to pid - 1 using MPI_send
          temp_row = &input_temp[1][0];
          MPI_Isend(temp_row, n+2, MPI_DOUBLE, (pid - 1), 0, MPI_COMM_WORLD, &send_req1);

          // sync with pid + 1
          // send results of penultimate row to pid + 1 using MPI_send
          temp_row = &input_temp[nrows - 2][0];
          MPI_Isend(temp_row, n+2, MPI_DOUBLE, (pid + 1), 0, MPI_COMM_WORLD, &send_req2);
          // update bottom ghost cells from pid + 1 using MPI_recv
          temp_row = &input_temp[nrows - 1][0];
          MPI_Irecv(temp_row, n+2, MPI_DOUBLE, (pid + 1), 0, MPI_COMM_WORLD, &recv_req2);
        }

      } else {

        if(not DEBUG) {

          // after last iteration send results back to master
          for (int k = 2; k < nrows; ++k) {
            temp_row = &output_temp[k][0];
            MPI_Send(temp_row, n+2, MPI_DOUBLE, MASTER, 0, MPI_COMM_WORLD);
          }

        }

      }

    }


  } else {

    MPI_Status status;

    // track completion of MPI message passing
    MPI_Request recv_req, send_req;

    // last process includes remainder of (n+2) / nprocs
    // also one extra row for ghost cells
    int nrows = floor( (n+2) / nprocs ) + ( (n+2) % nprocs ) + 1;
    Block input_temp(nrows, std::vector<double> (n+2, 0));
    Block output_temp(nrows, std::vector<double> (n+2, 0));
    if(DEBUG) std::cout << "proc " << pid << " nrows " << nrows << std::endl;

    // initialize values

    for (i = 0; i < nrows; ++i) {
      for (j = 0; j < n+2; ++j) {
        if ( i == (nrows - 1) || j == 0 || j == n+1 ) {
          input_temp[i][j] = boundary_val;
          output_temp[i][j] = boundary_val;
        } else {
          input_temp[i][j] = inner_val;
        }
      }
    }

    // solve for subarray

    for (int iter = 0; iter < niters; ++iter) {

      // no need to solve for first row (ghost cells)
      // or last row (boundary)
      if(iter > 0)
        MPI_Wait(&recv_req,&status); // wait on ghost cells
      for (i = 1; i < (nrows - 1); ++i) {
        for (j = 1; j < n+1; ++j) {
          output_temp[i][j] =
            input_temp[i][j] + k *
            (input_temp[i+1][j] + input_temp[i-1][j] -
             ( 4 * input_temp[i][j] ) +
             input_temp[i][j+1] + input_temp[i][j-1]
            );
        }
      }

      // no need for send or receive on last iteration
      if (iter < (niters - 1) ) {

        // swap temp matrices
        for(i = 0; i < nrows; ++i) {
          std::vector<double> &v1 = input_temp[i];
          std::vector<double> &v2 = output_temp[i];
          v1.swap(v2);
        }


        if(not DEBUG) {
          // sync with pid - 1
          // update top ghost cells from pid - 1 using MPI_recv
          temp_row = &input_temp[0][0];
          if (DEBUG) std::cout << "PID " << pid << " Recv row" << std::endl;
          MPI_Irecv(temp_row, n+2, MPI_DOUBLE, (pid - 1), 0, MPI_COMM_WORLD, &recv_req);
          // send results of 2nd row to pid - 1 using MPI_send
          temp_row = &input_temp[1][0];
          if (DEBUG) std::cout << "PID " << pid << " Send row" << std::endl;
          MPI_Isend(temp_row, n+2, MPI_DOUBLE, (pid - 1), 0, MPI_COMM_WORLD, &send_req);
          if (DEBUG) std::cout << "PID " << pid << " Send row DONE" << std::endl;

        }

      } else {

        if(not DEBUG) {
          // after last iteration send results back to master
          for (int k = 2; k < nrows; ++k) {
            temp_row = &output_temp[k][0];
            if (DEBUG) std::cout << "PID " << pid << " Send row " << k << "to Master" << std::endl;
            MPI_Send(temp_row, n+2, MPI_DOUBLE, MASTER, 0, MPI_COMM_WORLD);
          }


        }

      }

    }


  }

  MPI_Finalize();
  return 0;

}
