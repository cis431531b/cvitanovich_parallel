// CIS 431 Assignment 1
// Andrew Cvitanovich

// OSX compilation:
//    g++ -I/opt/X11/include -o grayscale grayscale.cpp -L/opt/X11/lib -lX11 -ljpeg

#include <cstdio>
#include <iostream>

#define cimg_use_jpeg 1
#include "CImg.h"
#include "jpeglib.h"

// timer function using chrono library
// https://en.cppreference.com/w/cpp/chrono
#include <ctime>
#include <chrono>

#include <cmath>
#define BLUR_RADIUS 3
#define PI 3.14159265359

// Adapted from examples at http://blog.ivank.net/fastest-gaussian-blur.html
void gaussblur (float input[], float output[], int r, int w, int h) {
	int start;
	int x = 0;
	int y = 0;
	
    float rs = (float) ceil(r * 2.57);     // significant radius

	// loop through columns
	for(int i=0; i<h; i++) {
		for(int j=0; j<w; j++) {
			
			// gaussian blur of pixel at coordinates (i,j)
			
			for (int channel = 0; channel < 3; channel++) {
				
				// start index
				start = channel*w*h;
				
				float val = 0, wsum = 0;
				for(int iy = i-rs; iy<i+rs+1; iy++) {
					for(int ix = j-rs; ix<j+rs+1; ix++) {
						x = fmin(w-1, fmax(0, ix));
						y = fmin(h-1, fmax(0, iy));
						float dsq = (ix-j)*(ix-j)+(iy-i)*(iy-i);
						float wght = exp( -dsq / (2*r*r) ) / (PI*2*r*r);
						val += input[start + y*w + x] * wght;  wsum += wght;
					}
				}
				
				// save result to output
				output[start + i*w + j] = val / wsum;
			}
		}
	}
}

int main() {

  // import image from jpg file
  cimg_library::CImg<float> input_img("bombus.jpg");

  // create new image (width, height, depth, channels (RGB))
  cimg_library::CImg<float> output_img(
      input_img.width(), input_img.height(), 1, 3);

  // dump pixel data
  float * in = input_img.data();
  float * out = output_img.data();
  
  // width and height
  
  int width = input_img.width();
  int height = input_img.height();
  
  // start timer
  auto start = std::chrono::system_clock::now();
  
  // apply gaussian blur with radius of BLUR_RADIUS pixels
  gaussblur ( in, out, BLUR_RADIUS, width, height );
  
  // stop timer
  auto end = std::chrono::system_clock::now();

  
  // print timer results
  std::chrono::duration<double> elapsed_seconds = end-start;
  std::cout << "Elapsed time for distortion: " << elapsed_seconds.count() << "s" << std::endl;
  
  output_img.save_jpeg("output.jpg");
}
