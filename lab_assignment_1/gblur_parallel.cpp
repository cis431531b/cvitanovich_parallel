// CIS 431 Assignment 1
// Andrew Cvitanovich

// OSX compilation:
//    g++ -I/opt/X11/include -o grayscale grayscale.cpp -L/opt/X11/lib -lX11 -ljpeg

#include <cstdio>
#include <iostream>

#define cimg_use_jpeg 1
#include "CImg.h"
#include "jpeglib.h"

// timer function using chrono library
// https://en.cppreference.com/w/cpp/chrono
#include <ctime>
#include <chrono>

#include <cmath>
#define BLUR_RADIUS 3
#define PI 3.14159265359

// parallelized with ispc
#include "vec_blur_ispc.h"

int main() {

  // import image from jpg file
  cimg_library::CImg<float> input_img("bombus.jpg");

  // create new image (width, height, depth, channels (RGB))
  cimg_library::CImg<float> output_img(
      input_img.width(), input_img.height(), 1, 3);

  // dump pixel data
  float * in = input_img.data();
  float * out = output_img.data();
  
  // width and height
  
  int width = input_img.width();
  int height = input_img.height();
  
  // start timer
  auto start = std::chrono::system_clock::now();
  
  // apply gaussian blur with radius of BLUR_RADIUS pixels
  ispc::vec_blur ( in, out, BLUR_RADIUS, width, height );
  
  // stop timer
  auto end = std::chrono::system_clock::now();

  
  // print timer results
  std::chrono::duration<double> elapsed_seconds = end-start;
  std::cout << "Elapsed time for distortion: " << elapsed_seconds.count() << "s" << std::endl;
  
  output_img.save_jpeg("output.jpg");
}
