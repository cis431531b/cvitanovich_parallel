#! /bin/bash

for i in 100 1000 10000
do
	for j in 1 2 4 8 16
	do

		echo "Testing n = $i and p = $j"
		export OMP_NUM_THREADS=$j
		./heat_solver_parallel $i
		echo ; echo	
	done
done
	
