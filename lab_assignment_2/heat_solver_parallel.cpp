// Assign 2 -- parallel version -- Andrew Cvitanovich
#include <vector>
#include <chrono>
#include <iostream>
#include <iomanip>
#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#define ITER_COUNT 10000
#define N 10000

int main(int argc, char *argv[]) {

  if(argc < 2) {
	std::cout << "Must provide array size n as first argument!\n";
	exit(0);
  }

  int n = atoi(argv[1]); // rows, cols of nxn input
  std::cout << "n = " << n << " ";
  int i,j;

  // dynamic 2D arrays
  std::vector< std::vector<double>> input_temp(n+2, std::vector<double> (n+2, 0));
  std::vector< std::vector<double>> output_temp(n+2, std::vector<double> (n+2, 0));

  double c = 0.1;
  double ds = 1 / ((double) n + 1);
  double dt = (ds * ds) / (4 * c);
  double k = c * (dt / (ds * ds));
  double boundary_val = 100;
  double inner_val = -100;
  int niters = ITER_COUNT;
  int thread_count;

  typedef std::chrono::high_resolution_clock Clock;

  // initialize boundary values for input_temp and output_temp (100)
  // initialize inner values for input_temp (-100)
  for (i = 0; i < n+2; ++i) {
    for (j = 0; j < n+2; ++j) {
      if( i == 0 || i == n+1 || j == 0 || j == n+1) {
        input_temp[i][j] = boundary_val;
        output_temp[i][j] = boundary_val;
      } else {
        input_temp[i][j] = inner_val;
      }
    }
  }

  // solve 2D heat equation iteratively
  auto t1 = Clock::now();
  for (int iter = 0; iter < niters; ++iter) {


      #pragma omp parallel for schedule(static,125) shared(input_temp,output_temp,n,k) private(i,j)
      for (i = 1; i < n+1; ++i) {
	if(i==1)
		thread_count = omp_get_num_threads();
	for (j = 1; j < n+1; ++j) {
	   output_temp[i][j] =
            input_temp[i][j] + k *
            (input_temp[i+1][j] + input_temp[i-1][j] -
             ( 4 * input_temp[i][j] ) +
             input_temp[i][j+1] + input_temp[i][j-1]
            );

        }
      }

      // swap temp matrices
      #pragma omp parallel for schedule(static,125) shared(input_temp,output_temp,n) private(i)
      for(i = 0; i < n+2; ++i) {
        std::vector<double> &v1 = input_temp[i];
        std::vector<double> &v2 = output_temp[i];
        v1.swap(v2);
      }

  }
  auto t2 = Clock::now();

  std::cout << "p = " << thread_count << std::endl;

  // write output_temp?
  std::cout << "Completed:\n\n";
  double asum = 0;
  std::cout << std::setprecision(10);
  for (i = 1; i < n+1; i++) {
    for (j = 1; j < n+1; j++) {
      asum += output_temp[i][j];
    }
  }

  // print sum of final array
  std::cout << "SUM = " << asum << std::endl;


  std::cout << "Runtime: "
  << std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count()
  << " ms" << std::endl;

}
